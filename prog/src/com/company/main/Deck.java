package com.company.main;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private int nbCard;
    public ArrayList<ACard> cards;
    ACard.Kind kind;


    public Deck(int _nbCard){
        this.nbCard = _nbCard;
        cards = new ArrayList<ACard>();
        setDeckCard();
    }

    Deck(){
        cards = new ArrayList<ACard>();
        setDeckCard();
    }

  public  ACard getCard() {
      int randTemp = 0;
      GameCard tempcard=null;
      boolean gotTrue =false;
      for(ACard cd: cards){
          if(cd.pickable){
              gotTrue =true;
          }
      }

    if(gotTrue){
          do {

              randTemp = new Random().nextInt(this.nbCard);

              if (cards.get(randTemp).pickable) {

                  cards.get(randTemp).pickable = false;
                  tempcard = (GameCard) cards.get(randTemp);

              }
          } while (cards.get(randTemp).pickable);
          return tempcard;
      }
        else{
              return null;
          }
    }

   public void setDeckCard(){
    ACard.colorKind color ;
       GameCard tempoCard;
       if(nbCard == 108){
            //creaation du de 2 deck de cartes
            for(int z = 0 ; z<2; z++) {
                for (int i = 0; i < 4; i++) {
                    if(i<2)
                    {
                        color = ACard.colorKind.red;
                    }
                    else
                    {
                        color = ACard.colorKind.black;
                    }
                    for (int j = 1; j <= 13; j++) {

                        tempoCard = new GameCard(color,kind.values()[i],j);
                        cards.add(tempoCard);
                       // System.out.println(tempoCard.pickable);

                    }
                }
            }

            for(int i=0;i<4;i++){
                tempoCard = new GameCard(ACard.colorKind.any,kind.joker,0);

                cards.add(tempoCard);
                //System.out.println(tempoCard.kind.toString());
            }

        }
       else{
           //prog deck
           YellowCard temp;
           for (int i = 0 ;i<nbCard;i++){
                temp = new YellowCard();
                cards.add(temp);
           }

       }

   }

}
