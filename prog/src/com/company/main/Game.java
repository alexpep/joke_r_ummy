package com.company.main;

import java.util.ArrayList;

public class Game implements Observer {
    public boolean gameDone;
    public Deck gameCardDeck ;
    public Stack stack ;
    public Deck yellowCardDeck;
    public ArrayList<Player> players ;

    public Game(){
        gameDone= false;
        players = new ArrayList<Player>();
        gameCardDeck = new Deck(108);
        stack = new Stack(gameCardDeck);
        yellowCardDeck = new Deck(50);

    }

    public void update(Player p) {
        if(p.getNbCompleted() == 5){
            gameDone =true;
        }
    }

}
