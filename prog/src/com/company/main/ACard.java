package com.company.main;

import javax.print.DocFlavor;

public abstract class ACard {
    boolean  pickable;

    public enum Kind{
        diamonds, hearts,spades, clubs, any,joker
   }
    protected Kind kind;
    public enum colorKind{red, black, any};
    protected colorKind color;
    ACard(colorKind _color, Kind _kind){
        pickable = true;
        this.color = _color;
        this.kind = _kind;

    }

    ACard(){
        pickable =true;
    }
}
