package com.company.main;

public class Stack extends Deck {
    private ACard firstCard;

    Stack(Deck deck) {
        super();
        firstCard = deck.getCard();
    }

    public void setFirstCard(GameCard firstCard) {
        this.firstCard = firstCard;
    }

    @Override
    public ACard getCard() {
        return firstCard;
    }
}
