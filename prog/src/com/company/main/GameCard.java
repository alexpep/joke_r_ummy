package com.company.main;

public class GameCard extends ACard {
    private int value;

    public GameCard(colorKind _color, Kind _kind, int _value){
        super( _color,  _kind);
        this.value = _value;

    }

    public void setColor(colorKind color) {
        this.color = color;
    }

    public colorKind getColor() {
        return color;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Kind getKind() {
        return kind;
    }

    public void setKind(Kind kind) {
        this.kind = kind;
    }
}
