package com.company.main;

import java.util.ArrayList;

public class Player implements Observable {
    private Hand hand;
    private YellowCard progCard;
    private int nbCompleted;
    private ArrayList<Observer> observers;

    public Player(){
        hand= new Hand();
        nbCompleted = 0;
        progCard = null;
        observers = new ArrayList<Observer>();
    }

    public void addObserver(Observer obs){
            observers.add(obs);
    }

    public void deleteObserver(Observer obs){
        if(observers.contains(obs)) {
            observers.remove(obs);
        }
    }

    public void notifyObserver(){
        if(observers.size()>0) {
            for (Observer obs : observers) {
                obs.update(this);
            }
        }
    }

    public void completedCard(){
    //System.out.println("Player nb card hand" + hand.size());
        if(progCard.validation(hand)){
            nbCompleted ++;
            notifyObserver();
        }
    }


    public int getNbCompleted() {
        return nbCompleted;
    }

    public YellowCard getProgCard() {
        return progCard;
    }

    public void setProgCard(YellowCard progCard) {
        this.progCard = progCard;
    }

    public Hand getHand() {
        return hand;
    }

    public ArrayList<Observer> getObservers() {
        return observers;
    }
}
