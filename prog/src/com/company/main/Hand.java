package com.company.main;

import java.util.ArrayList;

public class Hand {
    private ArrayList<GameCard> cards;


    public Hand(){

        cards = new ArrayList<GameCard>();
    }

    public void pickCard(Deck d){
        cards.add((GameCard) d.getCard());
    }



    public GameCard discardCard(GameCard gcard){
        if(cards.contains(gcard)) {
            cards.remove(gcard);
        }
        return gcard;
    }

    public ArrayList<GameCard> getCards() {
        return cards;
    }

    public void setCards(GameCard gc){
        cards.add(gc);
    }
}
