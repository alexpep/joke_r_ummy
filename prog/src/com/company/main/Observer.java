package com.company.main;

public interface Observer {
   void update(Player p);
}
