package com.company.main;

import java.util.ArrayList;

public class Main {


    public static void main(String[] args) {
        Game game = new Game();
        int nbplayer= 5;
        int randTemp =0;
        int winner=0;

        for (int i=0; i < nbplayer;i++){
            Player player = new Player();
            game.players.add(player);

            for(int j = 0; j<13;j++) {
                game.players.get(i).getHand().pickCard(game.gameCardDeck);
            }
        }
       // System.out.print(game.players.get(0).getHand().size());

        for(Player p: game.players){
            p.addObserver(game);

        }
        int b =0;

        while(!game.gameDone){
            for(Player p:game.players){
                    b=0;
                for(ACard c:game.gameCardDeck.cards){
                    if(c.pickable){
                        b++;
                    }
                }

                if(b>0) {
                    p.getHand().pickCard(game.gameCardDeck);
                }
                else
                {
                    game.gameDone=true;
                }


                if(p.getHand().getCards().contains(null)){
                    game.gameDone =true;
                }

                if(p.getProgCard()!=null) {
                    p.completedCard();
                }
                else{
                 p.setProgCard((YellowCard) game.yellowCardDeck.getCard());
                }

               if(p.getProgCard()!=null &&  p.getProgCard().getCompleted() && p.getNbCompleted() < 5){

                   p.setProgCard((YellowCard) game.yellowCardDeck.getCard());
                   if(p.getProgCard()== null){
                       game.gameDone = true;
                   }
               }
               if(p.getHand().getCards().size()>0) {
                   p.getHand().discardCard(p.getHand().getCards().get(p.getHand().getCards().size() - 1));
               }

            }

        }



        for(int i=1;i<game.players.size() ; i++){
            if(game.players.get(winner).getNbCompleted() < game.players.get(i).getNbCompleted())
            {
                winner= i;

            }
        }

        for(Player p:game.players){
            System.out.println(p.getNbCompleted()+"\n");
        }


        System.out.println("the winner is player " + (winner+1));



    }
}
