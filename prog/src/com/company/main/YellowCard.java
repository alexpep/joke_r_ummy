package com.company.main;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class YellowCard extends ACard {
    private  ArrayList<GameCard> cardNeed;
    private boolean completed;
    private ArrayList<Boolean> valide = new ArrayList<Boolean>();
    private int min=3;
    private int max=6;

    private enum progKind{straight , xOfKind}

    public YellowCard(){
        super();
        completed = false;
        cardNeed = new ArrayList<GameCard>();
        createProg();

    }

    public boolean validation(Hand hand){
        int validation = 0;
        int freeCard=0;


       // System.out.println(cardNeed.size());
       // System.out.println(hand.size());
        for(int i=0;i<cardNeed.size() ; i++) {
            for(GameCard hd: hand.getCards()){

                if(cardNeed.get(i).getValue() == hd.getValue()){

                     if((cardNeed.get(i).color != colorKind.any && cardNeed.get(i).color == hd.color) || cardNeed.get(i).color == colorKind.any){

                         validation++;
                     }

                     if((cardNeed.get(i).kind != Kind.any && cardNeed.get(i).kind == hd.kind) || cardNeed.get(i).kind == Kind.any){
                         validation++;
                     }

                     if(hd.kind ==  Kind.joker || hd.getValue() == 2){
                         freeCard++;
                     }


                    if(validation == 2) {

                        valide.add(true);
                    }
                }
                validation = 0;
            }
        }

        freeCard = freeCard/hand.getCards().size();

        if(valide.size() == cardNeed.size() || valide.size()+freeCard == cardNeed.size()){

            this.completed =true;
        }
        valide.removeAll(valide);


        System.out.println("quit val");
        return completed;
    }

    private void createProg(){
        int kindProg =  new Random().nextInt(2);

        int size = new Random().nextInt((max - min)+1) + min;
        colorKind color = colorKind.values()[new Random().nextInt(colorKind.values().length)];
        int kindCard = new Random().nextInt(ACard.Kind.values().length -1);
        int valueCard;
        do{
            valueCard = new Random().nextInt(10) + 3;

        }while(valueCard - size ==1 || valueCard - size ==2 );


        this.kind = Kind.values()[kindCard];
        this.color = color;
        GameCard gcard;

        if(kindProg !=1){
            //suite
            if(valueCard + size <=13){
                for(int i = valueCard; i<valueCard+size;i++){
                    gcard = new GameCard(color,this.kind,valueCard);
                    cardNeed.add(gcard);
                }
            }
            else{
                for(int i = valueCard; i>valueCard-size;i--){
                    gcard = new GameCard(color,this.kind,valueCard);
                    cardNeed.add(gcard);
                }
            }

        }
        else{
            //identique
            for(int i = 0; i<size;i++){
                gcard = new GameCard(color,this.kind,valueCard);
                cardNeed.add(gcard);
            }
        }


    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean getCompleted(){
        return completed;
    }
}
