package com.company.test;

import com.company.main.*;
import com.company.main.ACard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    @org.junit.jupiter.api.Test
    void addObserver() {
        Player p = new Player();
        Game g = new Game();
        p.addObserver(g);

        assertEquals(1,p.getObservers().size());
    }

    @org.junit.jupiter.api.Test
    void deleteObserver() {
        Player p = new Player();
        Game g = new Game();
        Game f = new Game();
        p.addObserver(g);
        p.addObserver(f);
        p.deleteObserver(g);

        assertEquals(1,p.getObservers().size());
    }
    


    @Test
    void consPlayer(){
        Player p = new Player();

        assertNotNull(p.getObservers());
        assertEquals(0,p.getNbCompleted());
        assertNull(p.getProgCard());
        assertNotNull(p.getHand());

    }





}