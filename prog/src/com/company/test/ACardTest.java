package com.company.test;

import com.company.main.ACard;
import com.company.main.GameCard;
import com.company.main.YellowCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ACardTest {


    @Test
    void getKindCardReturnEnumKind(){
        GameCard gc = new GameCard(ACard.colorKind.any, ACard.Kind.diamonds,3);

        assertEquals(ACard.colorKind.any,gc.getColor());

    }

    @Test
    void gameCardIsACard(){
        GameCard gc = new GameCard(ACard.colorKind.any, ACard.Kind.diamonds,3);
        assertEquals(ACard.class,gc.getClass().getSuperclass());
    }

    @Test
    void yellowCardIsAcard(){
        YellowCard gc = new YellowCard();
        assertEquals(ACard.class,gc.getClass().getSuperclass());
    }
}