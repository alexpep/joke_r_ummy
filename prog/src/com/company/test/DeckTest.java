package com.company.test;

import com.company.main.ACard;
import com.company.main.Deck;
import com.company.main.GameCard;
import com.company.main.YellowCard;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {



    @Test
    void setDeckCardSizeDeckGameCard() {
        Deck deck = new Deck(108);

        assertEquals(108,deck.cards.size());
    }

    @Test
    void deckGotGameCard() {
        Deck deck = new Deck(108);
        GameCard gc = new GameCard(ACard.colorKind.any, ACard.Kind.hearts,3);

        assertEquals(gc.getClass(),deck.cards.get(0).getClass());
    }

    @Test
    void setDeckCardSizeDeckYellowCard() {
        Deck deck = new Deck(50);

        assertEquals(50,deck.cards.size());
    }

    @Test
    void deckGotYellowCard() {
        Deck deck = new Deck(50);
        YellowCard yd = new YellowCard();

        assertEquals(yd.getClass(),deck.cards.get(0).getClass());
    }

}